gem 'pdf-reader'
require 'pdf-reader'
require 'open3'

def main
  pdf_dir = ARGV[0] + '*.pdf'
  puts pdf_dir

  print 'enter in the search terms: '
  search_terms = STDIN.gets.strip
  Dir[pdf_dir].each do |pdf|
    reader = PDF::Reader.new pdf
    reader.pages.each do |p|
      if p.text.include? search_terms
        puts pdf
        Open3.popen3('evince', pdf)
        break
      end
    end
  end
end

if __FILE__ == $0
  main
end