RudePDFSearch
=============

This is a simple script used to search PDF in a directory.

Example Usage
-------------------

program.rb ~/Downloads/

From here you will be prompted for your next input.

----------------------------------------


Any pdf that matches your search terms will open with 'evince' on linux by default please make sure this is installed

Dependencies
------------------------

pdf view evince must be installed. This can be installed with your pacage manage of choice.

**Example:** apt-get install evince
